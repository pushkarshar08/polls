from django.http import HttpResponseRedirect,HttpResponse,JsonResponse
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.utils import timezone
from django.views import generic
from rest_framework.decorators import api_view
from rest_framework.parsers import JSONParser
from rest_framework.response import Response
from rest_framework import status

from .models import Choice, Question
from .serializers import QuestionSerializer,ChoiceSerializer


class IndexView(generic.ListView):
    template_name = 'polls/index.html'
    context_object_name = 'latest_question_list'

    def get_queryset(self):
        """
        Return the last five published questions (not including those set to be
        published in the future).
        """
        return Question.objects.order_by('-pub_date')[:5]
        #filter(
         #   pub_date__lte=timezone.now()
        #).
        


class DetailView(generic.DetailView):
    model = Question
    template_name = 'polls/detail.html'

    def get_queryset(self):
        """
        Excludes any questions that aren't published yet.
        """
        return Question.objects.filter(pub_date__lte=timezone.now())


class ResultsView(generic.DetailView):
    model = Question
    template_name = 'polls/results.html'


def vote(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    try:
        selected_choice = question.choice_set.get(pk=request.POST['choice'])
    except (KeyError, Choice.DoesNotExist):
        return render(request, 'polls/detail.html', {
            'question': question,
            'error_message': "You didn't select a choice.",
        })
    else:
        selected_choice.votes += 1
        selected_choice.save()
        return HttpResponseRedirect(reverse('polls:results', args=(question.id,)))

#The root of our API is going to be a view that supports all the existing Questions or creating a new question

@api_view(['GET','POST'])
def question_list(request):
    #list all question or create a new question
    if request.method =='GET':
        questions=Question.objects.all()
        serializer=QuestionSerializer(questions,many=True)
        return Response(serializer.data)

    elif request.method=='POST':
        #data=JSONParser().parse(request)
        serializer=QuestionSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status=status.HTTP_201_CREATED)
        return JsonResponse(serializer.errors,status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET','PUT','DELETE'])
def question_detail(request,pk):
    #retieve,update or delete
    try:
        question=Question.objects.get(pk=pk)
    except Question.DoesNotExist:
        return HttpResponse(status=status.HTTP_404_NOT_FOUND)

    if request.method=='GET':
        serializer=QuestionSerializer(question)
        return Response(serializer.data)

    elif request.method=='PUT':
        #data=JSONParser().parse(request)
        serializer=QuestionSerializer(question,data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)

    elif request.method=='DELETE':
        question.delete()
        return HttpResponse(status=status.HTTP_204_NO_CONTENT)

@api_view(['GET','POST'])
def choice_list(request):
    #list all question or create a new question
    if request.method =='GET':
        choices=Choice.objects.all()
        serializer=ChoiceSerializer(choices,many=True)
        return Response(serializer.data)

    elif request.method=='POST':
        #data=JSONParser().parse(request)

        serializer=ChoiceSerializer(data=request.data)
        print(request.data)
        print(request.POST)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status=status.HTTP_201_CREATED)
        return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)

