from rest_framework import serializers
from .models import Question,Choice
import datetime
from django.utils import timezone

"""class QuestionSerializer(serializers.Serializer):
    id=serializers.IntegerField(read_only=True)
    question_text=serializers.CharField(max_length=300)
    pub_date=serializers.DateTimeField()

    def create(self,validated_data):
        #create and return a new Question instance given the validated data
        return Question.objects.create(**validated_data)

    def update(self,instance,validated_data):
        #update and return an existing Question instance
        instance.question_text=validated_data.get('question_text',instance.question_text)
        instance.save()
        return instance
"""

class ChoiceSerializer(serializers.ModelSerializer):
    def to_internal_value(self, data):
        return data

    class Meta:
        model=Choice
        fields=['question_id','choice_text']
        depth=1

class QuestionSerializer(serializers.ModelSerializer):
    choices=ChoiceSerializer(many=True,read_only=True)
    class Meta:
        model=Question
        fields=['id','question_text','pub_date','choices']


 