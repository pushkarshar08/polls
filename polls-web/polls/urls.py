from django.urls import path

from . import views


app_name = 'polls'
urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('<int:pk>/', views.DetailView.as_view(), name='detail'),
    path('<int:pk>/results/', views.ResultsView.as_view(), name='results'),
    path('<int:question_id>/vote/', views.vote, name='vote'),
    path('api/',views.question_list,name='question-list'),
    path('api/<int:pk>/',views.question_detail,name='question-detail'),
    path('api/choices/',views.choice_list,name='choice-list'),
]
