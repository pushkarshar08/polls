import datetime
import json
from django.test import TestCase
from django.urls import reverse
from django.utils import timezone

from .models import Question,Choice


class QuestionModelTests(TestCase):

    def test_was_published_recently_with_future_question(self):
        #was_published_recently() returns False for questions whose pub_date is in the future.

        time = timezone.now() + datetime.timedelta(days=30)
        future_question = Question(pub_date=time)
        self.assertIs(future_question.was_published_recently(), False)


    def test_was_published_recently_with_old_question(self):
        #was_published_recently() returns False for questions whose pub_date is older than 1 day.
        
        time = timezone.now() - datetime.timedelta(days=1, seconds=1)
        old_question = Question(pub_date=time)
        self.assertIs(old_question.was_published_recently(), False)

    def test_was_published_recently_with_recent_question(self):
        #was_published_recently() returns True for questions whose pub_date is within the last day.
        
        time = timezone.now() - datetime.timedelta(hours=23, minutes=59, seconds=59)
        recent_question = Question(pub_date=time)
        self.assertIs(recent_question.was_published_recently(), True)

def create_question(question_text, days):
    #Create a question with the given `question_text` and published the date
    time = timezone.now() + datetime.timedelta(days=days)
    return Question.objects.create(question_text=question_text, pub_date=time)



class QuestionIndexViewTests(TestCase):
    
    def test_no_questions(self):
        #If no questions exist, an appropriate message is displayed.

        response = self.client.get(reverse('polls:index'))
        self.assertEqual(response.status_code, 200)
        #self.assertContains(response, "No polls are available.")
        self.assertQuerysetEqual(response.context['latest_question_list'], [])

    def test_past_question(self):
        #Questions with a pub_date in the past are displayed on the index page.
        
        question = create_question(question_text="Past question.", days=-30)
        response = self.client.get(reverse('polls:index'))
        self.assertQuerysetEqual(
            response.context['latest_question_list'],
            [question],
        )

#220413-000000
class Test_api(TestCase):
    def setUp(self):
        poll = Question(question_text = "hhihii")
        poll.save()
        poll= Question(question_text="abcdfgh")
        poll.save()

    def test_question_get_all(self):
        res=self.client.get(reverse('polls:question-list'))
        data=json.loads(res.content)
        self.assertEquals(res.status_code,200)

    def test_question_get_id(self):
        res=self.client.get(reverse('polls:question-detail',kwargs={'pk':'1'}))
        self.assertEquals(res.status_code,200)
        data=json.loads(res.content)
        self.assertEquals(data.get('question_text'),"hhihii")
    
    def test_question_get_with_wrong_id(self):
        response = self.client.get(reverse('polls:question-detail', args=[3]))
        self.assertEqual(response.status_code,404)

    def test_post_question(self):
        res=self.client.post(reverse('polls:question-list'),{"question_text":"abcdfgh"})
        data=json.loads(res.content)
        self.assertEquals(res.status_code,201)

    def test_question_post_empty(self):
        res=self.client.post(reverse('polls:question-list'),{})
        data=json.loads(res.content)
        self.assertEquals(res.status_code,400)

    def test_put_wrong_id(self):
        res=self.client.put(reverse('polls:question-detail',kwargs={'pk':'7'}),{"question_text":"Best Coding platform?"},content_type="application/json")
        #data=json.loads(res.content)
        self.assertEquals(res.status_code,404)

    def test_put_empty(self):
        res=self.client.put(reverse('polls:question-detail',kwargs={'pk':'2'}),{},content_type="application/json")
        data=json.loads(res.content)
        self.assertEquals(res.status_code,400)
    
    def test_correct_input_using_put(self):
        res=self.client.put(reverse('polls:question-detail',kwargs={'pk':'2'}),{"question_text":"abcdfgh"},content_type="application/json")
        #data=json.loads(res.content)
        self.assertEquals(res.status_code,200)

    def test_delete_question_with_wrong_id(self):
        res=self.client.delete(reverse('polls:question-detail',kwargs={'pk':'3'}))
        self.assertEquals(res.status_code,404)

    """def test_delete_question_without_id(self):
        res=self.client.delete(reverse('polls:question-detail'))
        self.assertEquals(res.status_code,405)
        """
    def test_delete_question_correct_input(self):
        res=self.client.delete(reverse('polls:question-detail',kwargs={'pk':'1'}))
        self.assertEquals(res.status_code,204)